const inputForm = document.createElement('form');
const input = document.createElement('input');
input.setAttribute('placeholder', 'Focuse on me');

const ghostDiv = document.createElement('div');
ghostDiv.className = 'ghost-div';

inputForm.append(input);
document.body.append(inputForm);

input.onfocus = () => {
  document.body.append(ghostDiv);
};

input.addEventListener('blur', () => {
  ghostDiv.remove();
});
